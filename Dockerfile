FROM python:3.7.7

RUN mkdir /app
WORKDIR /app

RUN pip install gunicorn
COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY UniBOCalendarApp ./UniBOCalendarApp

EXPOSE 5000:5000

CMD ["gunicorn", "--chdir", "UniBOCalendarApp", "-b", "0.0.0.0:5000", "app:app"]
