import requests
from ics import Calendar, Event
import datetime

class Room:
    def __init__(self, room: dict):
        self.address = room.get('des_indirizzo')
        self.floor = room.get('des_piano')
        self.building = room.get('des_edificio')
        try:
            self.lat = room.get('edificio').get('geo').get('lat')
        except AttributeError:
            self.lat = ""
        try:
            self.lng = room.get('edificio').get('geo').get('lng')
        except AttributeError:
            self.lng = ""

    @classmethod
    def empty_room(cls):
        cls.address = ""
        cls.floor = ""
        cls.building = ""
        cls.lat = ""
        cls.lng = ""
        return cls

class Lesson:
    def __init__(self, lesson: dict):
        self.id = lesson.get('cod_modulo')
        self.title = lesson.get('title', 'Lesson title').capitalize().strip()
        self.start_time = lesson.get('start', 'Lesson start')
        self.end_time = lesson.get('end', 'Lesson end')
        self.delta_time = lesson.get('time', 'Lesson delta time')
        self.teacher = lesson.get('docente', "Lesson teacher")
        self.remote = lesson.get('teledidattica', 'Remote')
        self.teams_url = lesson.get('teams', 'teams url unavailable')

        rooms = lesson.get('aule')
        if len(rooms) > 0:
            self.room = Room(rooms[0])
        else:
            self.room = Room.empty_room()

    def __hash__(self):
        return hash((self.id, self.title))

    def __eq__(self, obj):
        return isinstance(obj, Lesson) and obj.id == self.id

    def __ne__(self, obj):
        return not self == obj

    @property
    def event(self):
        event = Event()
        event.name = self.title
        # Problem with timezone. Bologna is 2 hours behind the ics default timezone
        # TODO: update library when 0.8 will be out
        #start_time = datetime.datetime.strptime(self.start_time, "%Y-%m-%dT%H:%M:%S") - datetime.timedelta(hours=2)
        #end_time = datetime.datetime.strptime(self.end_time, "%Y-%m-%dT%H:%M:%S") - datetime.timedelta(hours=2)
        #event.begin = start_time.strftime("%Y-%m-%dT%H:%M:%S")
        #event.end = end_time.strftime("%Y-%m-%dT%H:%M:%S")

        event.begin = self.start_time
        event.end = self.end_time

        event.description = "Teams: {}\nDocente: {}\nDurata: {}".format(self.teams_url, self.teacher, self.delta_time)
        if self.room:
            event.location = self.room.address

        return event

def get_lessons_from_url(url: str):
    print(f"Get lessons from: {url}")
    lessons_json = requests.get(url).json()
    return [Lesson(lesson_dict) for lesson_dict in lessons_json]

#def get_data(course_type: str, course_name: str, year: str, curricula: str = None):
#    url: str = ""
#    if curricula:
#        url = "https://corsi.unibo.it/"\
#                "{}/"\
#                "{}/"\
#                "orario-lezioni/"\
#                "@@orario_reale_json?"\
#                "anno={}&amp;"\
#                "curricula={}".format(course_type, course_name, year, curricula)
#    else:
#        url = "https://corsi.unibo.it/"\
#                "{}/"\
#                "{}/"\
#                "orario-lezioni/"\
#                "@@orario_reale_json?"\
#                "anno={}".format(course_type, course_name, year)
#
#    return get_lessons_from_url(url)

def get_data_from_course_url(course_url: str, year: str, curricula: str = None):
    # https://corsi.unibo.it/magistrale/informatica/orario-lezioni/@@orario_reale_json?anno=1&curricula=A58-000&start=2021-03-22&end=2021-03-29
    url: str = ""
    url_section = "timetable" if ("2cycle" in course_url) else "orario-lezioni"
    start_date = datetime.date.today().strftime("%Y-%m-%d")
    end_date = ""
    if curricula:
        url = course_url + "/{}/@@orario_reale_json?anno={}&amp;curricula={}&amp;start={}".format(url_section, year, curricula, start_date)
    else:
        url = course_url + "/{}/@@orario_reale_json?anno={}&amp;start={}".format(url_section, year, start_date)

    return get_lessons_from_url(url)

def filter_lesson(lessons: list, lessons_filter: list):
    return list(filter(lambda lesson: lesson.id in lessons_filter, lessons))

def get_calendar(lessons: list):
    calendar = Calendar()
    for lesson in lessons:
        calendar.events.add(lesson.event)
    return calendar
