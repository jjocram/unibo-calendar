from os import remove
from io import BytesIO
from secrets import token_urlsafe

from flask import Flask, render_template, request, send_file, app
import requests
from flask_cors import CORS

from UniBOCalendarApp.calendar_parser import filter_lesson, get_calendar, get_data_from_course_url
from UniBOCalendarApp.teaching_parser import get_courses
app = Flask(__name__)
CORS(app)

def get_teachings_from_course_url(url: str, year: str, curriculum: str = None):
    app.logger.info(f"URL: {url}, year: {year}, curriculum: {curriculum}")
    return set(get_data_from_course_url(url, year, curriculum))

#def get_teaching(course_type: str, course_name: str, year: str, curricula: str = None):
#    """Return the list of available teachings as a set because it is the
#    easiest way to get only once for teaching"""
#    teachings = get_data(course_type, course_name, year, curricula)
#    return set(teachings)

def get_calendar_temp_file(calendar):
    """Return a BytesIO object used as temporary file to be sent to the client"""
    calendar_temp_file = BytesIO()
    filename = token_urlsafe(8)+'.ics'
    lines = []
    with open(filename, 'a+') as tmp_file:
        tmp_file.write(str(calendar))
        tmp_file.seek(0)
        lines = tmp_file.readlines()
        # WORKAROUND for timezones (it should work only in italy)
        for i, line in enumerate(lines):
            if ('DTSTART' in line) or ('DTEND' in line):
                lines[i] = line.replace('Z', '')

    with open(filename, 'w') as tmp_file:
        tmp_file.writelines(lines)

    with open(filename, 'r') as tmp_file:
        calendar_temp_file.write(bytes(tmp_file.read(), 'utf-8'))

    calendar_temp_file.seek(0)
    remove(filename)

    return calendar_temp_file

def multiple_curricula(course_url: str):
    curricula_url = course_url+"/orario-lezioni/@@available_curricula"
    curricula_response = requests.get(curricula_url)
    if curricula_response.status_code == 200:
        return True, curricula_response.json()
    else:
        return False, None

@app.route('/', methods=['GET', 'POST'])
def index():
    course_name = request.form.get('course_name')
    course_url = request.form.get('course_url')
    course_year = request.form.get('year')
    curriculum = request.form.get('curriculum')
    teachings = [teaching[0] for teaching in list(filter(lambda key_value: key_value[1] == "t", zip(request.form.keys(), request.form.values())))]

    if course_url and course_year and teachings:
        if curriculum:
            filtered_lessons = filter_lesson(get_data_from_course_url(course_url, course_year, curriculum), teachings)
            calendar = get_calendar(filtered_lessons)
            return send_file(get_calendar_temp_file(calendar),
                             as_attachment=True,
                             attachment_filename="{}-{}-{}.ics".format(course_name,
                                                                          course_year,
                                                                          curriculum))
        else:
            filtered_lessons = filter_lesson(get_data_from_course_url(course_url, course_year), teachings)
            calendar = get_calendar(filtered_lessons)
            return send_file(get_calendar_temp_file(calendar),
                             as_attachment=True,
                             attachment_filename="{}-{}.ics".format(course_name,
                                                                       course_year))

    if course_url and course_year and curriculum:
        #print(course_name, course_url, course_year, curriculum)
        _, curricula = multiple_curricula(course_url)
        return render_template('index.html', course_name=course_name, course_url=course_url, course_year=course_year, multiple_curricula=True, curricula=curricula, curriculum_posted=curriculum, teachings=get_teachings_from_course_url(course_url, course_year, curriculum))

    if course_url and course_year:
        #print(course_name, course_url, course_year)
        there_are_curricula, curricula = multiple_curricula(course_url)
        if there_are_curricula:
            # Request which curricula
            return render_template('index.html', course_name=course_name, course_url=course_url, course_year=course_year, multiple_curricula=True, curricula=curricula)
        else:
            # Request which teachings 
            return render_template('index.html', course_name=course_name, course_url=course_url, course_year=course_year, multiple_curricula=False, teachings=get_teachings_from_course_url(course_url, course_year))

    return render_template('index.html', available_courses=get_courses())


if __name__ == "__main__":
    app.run(host="0.0.0.0")
