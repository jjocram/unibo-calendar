import requests
from bs4 import BeautifulSoup

class Course:
    def __init__(self, title, url):
        self.title = title
        self.url = url

    def __hash__(self):
        return hash((self.title, self.url))

def get_courses():
    to_ret = []
    for i in range(16): #16 because there are 16 sections in UniBO courses
        r = requests.get("https://www.unibo.it/it/didattica/corsi-di-studio/elenco?&schede={}".format(i))
        soup = BeautifulSoup(r.content, 'html.parser')
        for teaching in soup.find_all('a', 'umtrack'):
            to_ret.append(Course(teaching['data-title'], teaching['href']))
    return set(to_ret)
