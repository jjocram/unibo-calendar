# UniBO-Calendar
From UniBO website is not possible to download an `*.ics` file to save lessons in your calendar.

I wrote this very simple web-app to get your course timetable.

# Use it
The application is hosted on Heroku, follow this link to use it: [https://unibocalendar.herokuapp.com](https://unibocalendar.herokuapp.com)

# How to run
If you want, you can build it from the sources
## As Python program
1. Install the requirements with `pip`. You can do this inside a [virtual environment](https://virtualenv.pypa.io/en/stable/) or directly.
    
    ```bash
    pip install -r requirements.txt
    ```
2. Move into UniBOApp.
    
    ```bash
    cd UniBOCalendarApp
    ```
3. Execute the flask web-server
    
    ```bash
    flask run
    ```
4. Open a browser and go to [http://127.0.0.1:5000/](http://127.0.0.1:5000/)
5. Insert the requested data to get your calendar.

## With Docker
### Compiling the docker image
1. Compile the image

    ```bash
    docker build -t unibocalendar .
    ```

2. Run the container

    ```bash
    docker run -it -p 5000:5000 unibocalendar
    ```
3. Open a browser and go to [http://127.0.0.1:5000/](http://127.0.0.1:5000/)
4. Insert the requested data to get your calendar.

### Using image from Gitlab Container Registry 
0. To use the GitLab registry is necessary to login

    ```bash
    docker login registry.gitlab.com
    ```
1. Run the container

    ```bash
    docker run -it -p 5000:5000 registry.gitlab.com/jjocram/unibo-calendar
    ```
3. Open a browser and go to [http://127.0.0.1:5000/](http://127.0.0.1:5000/)
4. Insert the requested data to get your calendar.
